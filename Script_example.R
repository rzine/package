#### Installation du package Rzine

#install.packages("remotes")  
remotes::install_gitlab("rzine/package", host="https://gitlab.huma-num.fr/", force = TRUE) 
library(rzine)


#### Création d'une fiche Rzine vierge (+ répertoires et fichiers associés)
rmarkdown::draft(file = "Ma_fiche_rzine.Rmd",  
                 template="readrzine", 
                 package="rzine",
                 create_dir = TRUE,    
                 edit = FALSE)


#### Ajouter le référencement d'une ACTU sur Rzine.fr
add_news()

add_news(date = "2021-04-01",
         title = "Prochain séminaire du UserGroup français le 11 avril",
         subtitle = "Analyse de réseau avec R, en visioconférence",
         summary = "Le groupe d'utilisateur organise un séminaire sur l'analyse de réseau le 11 avril en visioconférence",
         authors = "jdupont",
         tags = c("user group", "séminaire", "réseau", "network"))

??add_news


#### Ajouter le référencement d'une RESSOURCE divers sur Rzine.fr
add_resource()

add_resource(date = "2021-04-01",
             title = "Introduction à l'analyse textuelle avec R",
             subtitle = "Initiniation à Quanteda",
             summary = "Manuel d'initiation à l'analyse textuelle avec Quanteda",
             authors = "Jeanne Dupont",
             type = "5", 
             thematic = "13",
             update = "0",
             language = "0",
             source = "0",
             url_source = "https://...",             
             url_code = "https://git...",
             tags = c("texte", "quanteda", "tm", "tokenisation", "lemmatisation", "corpus"))

??add_resource



#### Ajouter le référencement d'un.e AUTEUR.E sur Rzine.fr
add_author()

add_author(name="Harry Houdini",
           role= "Illusionniste",
           mail= "harry.houdini@poudlard.fr",
           organization= c("Poudlard", "FISM"),
           url_orga= c("https://Poudlard.fr", "https://FISM.com"),
           url_git= "https://rzine.fr",
           url_site = "https://rzine.fr",
           url_twitter = "https://rzine.fr", 
           url_youtube = "https://rzine.fr", 
           user_group = "Autres")

??add.author


#### Ajouter le référencement d'une fiche Rzine sur Rzine.fr
# Not Working without correct folder name
add_publi_rzine(folder= "My Rzine sheet",
                summary = "Initiation à l’analyse de corrélation avec la suite easystats. Du calcul de coefficient à la visualisation d’une corrélation",
                thematic = "10",
                tags = c("easystats", "Pearson", "Spearman", "Kendall", "see", "corrélogramme", "corrplot", "cor.test"))

??add_publi_rzine


####################################################
devtools::document()
roxygen2::roxygenise()






