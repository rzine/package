# Package Rzine

[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://lbesson.mit-license.org/)

## Pré-requis

Pour utiliser le package rzine, il peut être utile d'installer l'IDE [RStudio Desktop](https://www.rstudio.com/products/rstudio/#rstudio-desktop) sous licence [AGPL v3](https://www.gnu.org/licenses/agpl-3.0.fr.html) au préalable.

## Installation du package `rzine`

Pour installer le package rzine, exécuter les instructions suivantes dans votre **console** :
```r
install.packages("remotes")
remotes::install_gitlab("rzine/package", host = "https://gitlab.huma-num.fr/", force = TRUE)
library("rzine")
```

## Créer le *template* `readrzine`

Il s'agit du **template de document HTML utilisé pour la production de** [**fiche Rzine**](https://rzine.fr/publication_rzine/). Cependant, **ce template peut être utilisé (et personalisé) à souhait**.  
Créer votre répertoire de travail pour votre fiche Rzine, par exemple **dir_ma_fiche_rzine/**.

### Création en lignes de code R
Dans votre répertoire de travail, créer un fichier script (mon_fichier_script.R).  
Copier-coller les **lignes de code suivantes pour générer le template** dans votre fichier script R et puis l'exécuter :
*Veiller à personnaliser le nom du fichier rmarkdown (argument de `file`)*

```r
rmarkdown::draft(file = "index.Rmd", 
                 template = "readrzine", 
                 package = "rzine", 
                 create_dir = FALSE, 
                 edit = FALSE)
```

### Création avec l'IDE RStudio.
Dans votre répertoire de travail, cliquer sur *File/New File/R Markdown*, puis sélectionner le template `readrzine` :

![](man/figures/read_rzine_template.png)

Cela génère un répertoire contenant un ensemble de fichiers à la racine de votre répertoire de travail :

![](man/figures/files_folder.png)

Un fichier .Rmd a été créé à la racine de ce nouveau répertoire. Vous pouvez le ***kniter***... Voilà à quoi ressemble le **template vierge** :

![](man/figures/rzine.png)

## Exemple d'en-tête

```
---
title: "Titre fiche"
subtitle: "Sous-titre fiche"
date: "`r Sys.Date()`"
author: 
 - name: Premier Auteur.e
   affiliation: Affiliation_1, Affiliation_2
 - name: Second Auteur.e
   affiliation: Affiliation_1, Affiliation_2
image: "featured.png"   
logo: "figures/rzine.png"  
output:
  rzine::readrzine:
    highlight: kate
    number_sections: true
csl: Rzine_citation.csl
bibliography: biblio.bib
nocite: |
  @*
link-citations: true

# github: "rzine-reviews/MTA"
# gitlab: "gitlab.huma-num.fr/rzine/help_rzine_publication"
# doi: "10.5281/zenodo.4554776"
# licence: "by-nc-sa"

# Only Creative Commons Licence 
# 5 possible choices : "by-nd", "by", "by-nc-sa", "by-nc","by-sa"
```
Vous pouvez :

- **Personaliser le logo affiché** (paramètre *logo*)       
- **Supprimer (ou modifier) l'image d'illustration** (paramètre *image*)   
- **Ajouter un lien (badge) vers un dépôt GitHub** (paramètre *github*)    
- **Ajouter un lien (badge) vers un dépôt GiLab** (paramètre *gitlab*)    
- **Ajouter un DOI** (badge) avec le paramètre *doi* 
- **Ajouter une licence Creative Commons** (badge) avec le paramètre *licence*    

## Les fonctions

**[WORK IN PROGRESS]**

## Crédits

**Le template `readrzine` est très largement inspiré du template** [`readthedown`](https://github.com/juba/rmdformats#readthedown) **du package** [`rmdformats`](https://github.com/juba/rmdformats) **développé par** [**Julien Barnier**](https://github.com/juba). 

[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://lbesson.mit-license.org/)

